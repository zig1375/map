"use strict";

if (typeof Zig == 'undefined') {
    var Zig = {};
}
if (typeof Zig.Map == 'undefined') {
    Zig.Map = {};
}

(function(){

    Zig.Map.Core = function(width, height, center_x, center_y, region_width, region_height) {
        Zig.EventEmitter.call(this);

        region_width  = region_width  || 128;
        region_height = region_height || 128;

        this._options = {
            size : {
                width  : width,
                height : height
            },

            cell : {
                width  : region_width,
                height : region_height,

                cols : Math.ceil(width  / region_width)  + 8,  // Количество колонок
                rows : Math.ceil(height / region_height) + 8   // Количество рядов
            }
        };

        this._data = {};   // Тут хранится загруженная карта

        this.init(center_x, center_y);
    };

    Zig.Map.Core.prototype = {
        init : function(center_x, center_y) {
            var initRunTime = new Date();

            try {
                this._canvas = new Zig.Map.Canvas(this._options.size.width, this._options.size.height, this);
            } catch (e) {
                if (e !== Zig.Error.NoSupportCanvas) {
                    throw e;
                }

                return;
            }

            this._options.focused  = null;
            this._options.selected = null;
            this._rebuild_buffer   = true; // Флаг о том, что нужно перестроить буфер
            this._rebuild_buffer2  = true; // Флаг о том, что нужно перестроить буфер 2

            var count = 1, _afterAll = function() {
                if (--count == 0) {
                    console && console.log('init complete over ' + (new Date() - initRunTime) + 'ms');

                    // Запускаем рендеринг
                    this._canvas.render();
                }
            }.bind(this);

            count++;
            Zig.Map.Resources.add('img/space.jpg', _afterAll);

            this.goto(center_x, center_y, _afterAll);
        },

        goto : function(center_x, center_y, callback) {
            // Получаем из координат центра координаты углов нужной области
            var coords = {
                x1 : center_x - Math.ceil(this._options.cell.cols / 2),
                y1 : center_y - Math.ceil(this._options.cell.rows / 2)
            };

            coords.x2 = coords.x1 + this._options.cell.cols;
            coords.y2 = coords.y1 + this._options.cell.rows;

            this._options.selected = { x : center_x, y : center_y };

            this._options.pos = {
                //  Пиксельные координаты верхнего левого угла канваса!
                px : {
                    x : (coords.x1 + 4) * this._options.cell.width,
                    y : (coords.y1 + 4) * this._options.cell.height
                },

                // Смещение буффера
                offset : {
                    x : this._options.cell.width  * 4,
                    y : this._options.cell.height * 4
                },

                // Координаты
                coord : {
                    x : coords.x1,
                    y : coords.y1
                }
            };

            this._load_map(coords, function() {
                callback && callback();
                this.emit('change', [this._options.selected]);
            }.bind(this));
        },

        _get_ajax_map : function(coords, callback) {
            setTimeout(function(){
                // Генегируем ответ аякса
                var map = {};
                for(var x = Math.min(coords.x1, coords.x2); x <= Math.max(coords.x1, coords.x2); x++) {
                    for(var y = Math.min(coords.y1, coords.y2); y <= Math.max(coords.y1, coords.y2); y++) {
                        if (typeof map[x] == 'undefined') {
                            map[x] = {};
                        }

                        if (x < 0 || y < 0) {
                            // пустота (море, пустыня, космос, на ваше усмотрение)
                            map[x][y] = { image : null };
                        } else {
                            map[x][y] = { image : 'img/' + (((y * 200 + x) % 7 + 2) + '.png') };
                        }
                    }
                }

                callback && callback(map);
            }.bind(this), 0);
        },

        _load_map : function(coords, callback, test) {
            // ToDo добавить проверку на наличие запрошенной области, чтобы не грузить повторно
            // Заменить эту функию на получение по аяксу
            this._get_ajax_map(coords, function(map) {
                // А теперь парсим его

                var imgs = {};
                $.each(map, function(x, yy) {
                    $.each(yy, function(y, data) {
                        if (typeof this._data[x] == 'undefined') {
                            this._data[x] = {};
                        }

                        if (data.image == null) {
                            // Якобы космос
                            this._data[x][y] = null;
                        } else {
                            if (!Zig.Map.Resources.has(data.image)) {
                                imgs[data.image] = data.image;
                            }

                            this._data[x][y] = new Zig.Map.Cell(data.image, x, y, this._options.cell);
                        }
                    }.bind(this));
                }.bind(this));

                // Загружаем картинки
                Zig.Map.Resources.add(imgs, function() {
                    this._rebuild_buffer = true;
                }.bind(this));

                this._rebuild_buffer = true;

                if (typeof callback == 'function') {
                    callback(coords);
                } else {
                    // ToDo сделать дефолтный вызов
                }
            }.bind(this));
        },

        _rebuildBuffer : function(buffer) {
            var cell    = this._options.cell,
                pos     = this._options.pos,
                left    = pos.coord.x * cell.width,
                top     = pos.coord.y * cell.height;

            // Находим отображаемый диапазон
            this._canvas.clearBuffer();

            for(var x = pos.coord.x; x <= pos.coord.x + cell.cols; x++) {
                for(var y = pos.coord.y; y <= pos.coord.y + cell.rows; y++) {
                    if ((typeof this._data[x] != 'undefined') && (typeof this._data[x][y] != 'undefined')) {
                        if (this._data[x][y] !== null) {
                            this._data[x][y].draw(buffer, left, top);
                        }
                    }
                }
            }
        },

        _rebuildBuffer2 : function(buffer) {
            this._canvas.clearBuffer2();

            var left = this._options.pos.coord.x * this._options.cell.width,
                top  = this._options.pos.coord.y * this._options.cell.height;

            if ((this._options.selected !== null) && (this._data[this._options.selected.x]) && (this._data[this._options.selected.x][this._options.selected.y])) {
                this._data[this._options.selected.x][this._options.selected.y].select(buffer, left, top);
            }

            if ((this._options.focused !== null) && (this._data[this._options.focused.x]) && (this._data[this._options.focused.x][this._options.focused.y])) {
                this._data[this._options.focused.x][this._options.focused.y].focus(buffer, left, top);
            }
        },

        _getXYByPx : function(px_x, px_y, need_4) {
            var y = Math.floor(px_y / this._options.cell.height),
                x = Math.floor(px_x / this._options.cell.width);

            return (need_4) ? {x : x - 4, y : y - 4} : {x : x, y : y};
        },

        _getXYByScreenPx : function(x, y, need_4) {
            var p = this._options.pos.px;
            return this._getXYByPx(x + this._options.cell.width + p.x, y + this._options.cell.height + p.y, need_4 || false);
        },

        _checkMoveMap : function(mouse) {
            var act = mouse.getAction();

            if (act.click) {
                this._options.selected = this._getXYByScreenPx(act.click.x, act.click.y);
                this.emit('change', [this._options.selected]);
                this._rebuild_buffer2 = true;
            }

            if (act.move) {
                this._options.focused = this._getXYByScreenPx(act.move.x, act.move.y);
                this._rebuild_buffer2 = true;
            }

            if (act.drag) {
                this._options.pos.offset.x += act.drag.x;
                this._options.pos.offset.y += act.drag.y;
                this._options.pos.px.x += act.drag.x;
                this._options.pos.px.y += act.drag.y;

                var p = this._options.pos,
                    xx = p.offset.x,
                    yy = p.offset.y,
                    w  = this._options.cell.width, h = this._options.cell.height;

                if ((xx <= w * 2) || (xx >= w * 6) || (yy <= h * 2) || (yy >= h * 6)) {
                    var xy = this._getXYByPx(p.px.x, p.px.y, true);

                    this._options.pos.coord = xy;
                    this._options.pos.offset.x = w * 4 + (p.px.x - (xy.x + 4) * w);
                    this._options.pos.offset.y = h * 4 + (p.px.y - (xy.y + 4) * h);

                    var way_x = (xx <= w * 2) ? -1 : ((xx >= w * 6) ? 1 : 0),
                        way_y = (yy <= h * 2) ? -1 : ((yy >= h * 6) ? 1 : 0);

                    this._rebuild_buffer = true;
                    this._checkNeedLoadMap(xy.x, xy.y, way_x, way_y);
                }
            }
        },

        _checkNeedLoadMap : function(xx, yy, way_x, way_y) {
            var x = [], y = [],
                map = this._data;

            if (way_x < 0) {
                x.push(xx - 20);
                x.push(xx + this._options.cell.cols);
            } else if (way_x > 0) {
                x.push(xx);
                x.push(xx + this._options.cell.cols + 20);
            } else if (way_x == 0) {
                x.push(xx - 10);
                x.push(xx + this._options.cell.cols + 10);
            }

            if (way_y < 0) {
                y.push(yy - 20);
                y.push(yy + this._options.cell.rows);
            } else if (way_y > 0) {
                y.push(yy);
                y.push(yy + this._options.cell.rows + 20);
            } else if (way_y == 0) {
                y.push(yy - 10);
                y.push(yy + this._options.cell.rows + 10);
            }

            for (var _x = 0; _x <= 1; _x++) {
                for(var _y = y[0]; _y <= y[1]; _y++) {
                    if ((!map[x[_x]]) || (!map[x[_x]][_y])) {
                        this._load_map({x1 : x[0], x2 : x[1], y1 : y[0], y2 : y[1]});
                        return;
                    }
                }
            }

            for(_y = 0; _y <= 1; _y++) {
                for (_x = x[0]; _y <= x[1]; _x++) {
                    if ((!map[_x]) || (!map[_x][y[_y]])) {
                        this._load_map({x1 : x[0], x2 : x[1], y1 : y[0], y2 : y[1]});
                        return;
                    }
                }
            }
        },

        render : function(buffer, buffer2, mouse) {
            this._checkMoveMap(mouse);

            if (this._rebuild_buffer) {
                // Перестраиваем буфер
                this._rebuild_buffer  = false;
                this._rebuild_buffer2 = false;

                this._rebuildBuffer(buffer);
                this._rebuildBuffer2(buffer2);
            } else if (this._rebuild_buffer2) {
                this._rebuild_buffer2 = false;
                this._rebuildBuffer2(buffer2);
            }

            return this._options.pos.offset;
        }
    };

    Zig.Utils.inherit(Zig.Map.Core, Zig.EventEmitter);

})();