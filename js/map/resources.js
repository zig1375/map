"use strict";

if (typeof Zig == 'undefined') {
    var Zig = {};
}
if (typeof Zig.Map == 'undefined') {
    Zig.Map = {};
}

(function(){

    Zig.Map.Resources = {
        _store   : {},
        _loading : {},

        add: function(url, callback) {
            if (typeof url == 'object') {
                $.each(url, function(k, v) {
                    this.add(v, callback || false);
                }.bind(this));

                return;
            }

            if (this._store[url]) {
                callback && callback();
                return;
            }

            if (!this._loading[url]) {
                var image = new Image();
                image.onload = function() {
                    this._loading[url].forEach(function(v){
                        v && v();
                    });

                    delete this._loading[url];
                }.bind(this);

                image.src = url;
                this._store[url] = image;
                this._loading[url] = [callback || false];
            } else if (callback) {
                this._loading[url].push(callback);
            }
        },

        get : function(url) {
            return this._store[url] || null;
        },

        has : function(url) {
            return !!this._store[url];
        }

    };

})();