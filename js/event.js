if (typeof  Zig == "undefined") {
    Zig = {};
}

Zig.EventEmitter = function() {
    this.handlers = [];
};

Zig.EventEmitter.prototype =
{
    addEventListener : function(event, callback, context, args) {
        if (typeof this.handlers[event] == "undefined") {
            this.handlers[event] = [];
        }
        this.handlers[event].push([context || null, callback, args || []]);

        return true;
    },

    removeListener : function(event, callback) {
        if (typeof this.handlers[event] == "undefined") {
            return false;
        }
        for (var i=0; i<this.handlers[event].length; i++) {
            if (this.handlers[event][i][1] == callback) {
                this.handlers[event].splice(i, 1);
                return true;
            }
        }
        return false;
    },

    emit : function(event, local_args) {
        if (typeof this.handlers[event] == "undefined") {
            this.handlers[event] = [];
        }

        local_args = local_args || [];

        for (var i=0; i<this.handlers[event].length; i++) {        
            var context = this.handlers[event][i][0] || window;        
            var callback = this.handlers[event][i][1];
            var args = this._cloneArguments(event, i);

            for (var j=local_args.length - 1; j > -1; j--) {
                args.unshift(local_args[j]);
            }
            // args.unshift(this);
            
            if (typeof callback == "string") {           
                context[callback].apply(context, args);
            } else {
                callback.apply(context, args);
            }               
        }
    },
    
    _cloneArguments : function(event, index) {
        var result = [];
        var event_arguments = this.handlers[event][index][2];

        for (var i=0; i<event_arguments.length; i++) {
            result.push(event_arguments[i]);
        }
        
        return result;
    },
    
    __dummy : null
};

